from django.shortcuts import render, redirect
from tasks.models import Task
from tasks.forms import CreateTask
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("show_project", id=task.project.id)
    else:
        form = CreateTask()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def my_task_list(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/my_list.html", context)
